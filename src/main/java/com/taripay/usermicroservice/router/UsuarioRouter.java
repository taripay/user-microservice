package com.taripay.usermicroservice.router;

import org.springframework.boot.autoconfigure.web.WebProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.taripay.usermicroservice.handler.UsuarioHandler;

@Configuration
public class UsuarioRouter {

	private String path = "api/usuario";
	
	 @Bean
	 public WebProperties.Resources resources(){
		 return new WebProperties.Resources();
	 }
	 
	 @Bean
	 RouterFunction<ServerResponse> routerUsuario(UsuarioHandler handler){
		 return RouterFunctions.route()
				 .GET(path, handler::login)
				 .GET(path + "/prop", handler::getTestProp)
				 .POST(path, handler::save)
				 .build();
	 }
}
