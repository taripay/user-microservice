package com.taripay.usermicroservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.taripay.usermicroservice.entity.Usuario;
import com.taripay.usermicroservice.repository.UsuarioRepository;

import reactor.core.publisher.Mono;

@Service
public class UsuarioService {
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	public Mono<Usuario> save(Usuario model){
		return usuarioRepository.save(model);
	}
	
	public Mono<Usuario> login(Usuario model){

		return usuarioRepository.findByUsuarioAndPassword("roberth", "123");
	}

}
