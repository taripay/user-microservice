package com.taripay.usermicroservice.repository;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

import com.taripay.usermicroservice.entity.Usuario;

import reactor.core.publisher.Mono;

@Repository
public interface UsuarioRepository extends ReactiveCrudRepository<Usuario, Long>{
	public Mono<Usuario> findByUsuarioAndPassword(String usuario, String password);
}
