package com.taripay.usermicroservice.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.taripay.usermicroservice.entity.Usuario;
import com.taripay.usermicroservice.service.UsuarioService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import reactor.core.publisher.Mono;

@Component
@Getter
@Setter
@RefreshScope
public class UsuarioHandler {
	 private final MediaType typeJson = MediaType.APPLICATION_JSON;
	 
	 @Autowired
	 UsuarioService usuarioService;
         
         @Value("${app.testProp}")
         private String testProp;
	 
	 public Mono<ServerResponse> save(ServerRequest request){
		 Mono<Usuario> usuario = request.bodyToMono(Usuario.class);
		 return usuario.flatMap(user -> ServerResponse.ok()
				 .contentType(typeJson)
				 .body(usuarioService.save(user), Usuario.class)
				 );
	 }
	 
	 public Mono<ServerResponse> login(ServerRequest request){
		 Mono<Usuario> usuario = request.bodyToMono(Usuario.class);
		 return usuario.flatMap(user -> ServerResponse.ok()
				 .contentType(typeJson)
				 .body(usuarioService.login(user), Usuario.class)
				 );
	 }
         
        public Mono<ServerResponse> getTestProp(ServerRequest request){

            return ServerResponse.ok()
                .contentType(typeJson)
                .body(Mono.just(this.testProp), String.class);
        } 
}
