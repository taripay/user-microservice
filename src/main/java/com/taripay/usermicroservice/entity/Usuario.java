package com.taripay.usermicroservice.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Table(value="tb_usuario")
@Getter
@Setter
@Builder
public class Usuario {

	@Id
	private Long id;
	private String usuario;
	private String password;
	private String nombre;
	private String telefono;
	private String correo;
	
	
	
}
